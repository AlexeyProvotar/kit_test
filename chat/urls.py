from django.conf.urls import patterns, include, url

from chat import views


urlpatterns = [

    url(r'^$', views.IndexPageView.as_view(template_name='public/index.html'), name='public_homepage'),

    url(r'^user/register/$', views.UserRegisterView.as_view(), name='public_user_register'),

    url(r'^user/register/complete/$', views.PublicPageView.as_view(template_name='public/registration_complete.html')),

    url(r'^user/login/$', views.UserLoginView.as_view(), name='public_user_login'),

    url(r'^user/$', views.ProtectedView.as_view(template_name='private/index.html'), name='private_homepage'),

    url(r'^private/user/logout', views.UserLogoutView.as_view(), name='private_user_logout'),

    # url('^accounts/', include('django.contrib.auth.urls'))

]
