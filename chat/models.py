from __future__ import unicode_literals

import hashlib
import random
import datetime

from django.db import models
from django.conf import settings
from django.utils import six, timezone
from django.core.validators import RegexValidator
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager


class UserManager(BaseUserManager):
    def create_user(self, username, email, first_name=None, last_name=None, password=None):
        if not email:
            raise ValueError('User must have a e-mail address')
        if not username:
            raise ValueError('User must have a username')

        user = self.model(
            username=username,
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name
        )

        user.is_active = True
        user.is_staff = True
        user.is_superuser = False

        user.set_password(password)

        return user

    def create_superuser(self, username, email, password):
        user = self.create_user(
            username=username,
            email=email,
            password=password,
            first_name=email.split('@')[0],
            last_name=email.split('@')[0]
        )

        user.is_active = True
        user.is_staff = True
        user.is_superuser = True

        user.save(using=self._db)

        return user


class UserModel(AbstractBaseUser, PermissionsMixin):

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    objects = UserManager()

    username = models.CharField(unique=True, max_length=255,
                                validators=[RegexValidator(r'^[0-9a-zA-Z]*$',
                                                           message='Only alphanumeric characters are allowed.')])
    email = models.EmailField(unique=True, max_length=255)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    date_joined = models.DateTimeField(default=timezone.now)
    is_active = models.BooleanField(default=True, null=False)
    is_staff = models.BooleanField(default=False, null=False)

    verify_key = models.CharField(max_length=255, null=True, blank=True)

    registered_at = models.DateTimeField(null=True)

    def get_full_name(self):
        full_name = self.first_name + ' ' + self.last_name

        return full_name

    def get_short_name(self):

        return self.username

    get_short_name.short_description = 'Short name'

    def __unicode__(self):
        return u'%s' % self.email

    def save(self, from_admin=False, *args, **kwargs):
        try:
            user = UserModel.objects.get(pk=self.pk)
        except:
            user = None

        if user and not self.registered_at:
            self.registered_at = timezone.now()

        super(UserModel, self).save(*args, **kwargs)

    class Meta:
        verbose_name = u'User'
        verbose_name_plural = u'Users'


class Message(models.Model):
    user = models.ForeignKey(UserModel)

    text = models.CharField('Message', max_length=255)

    created_at = models.DateTimeField(default=timezone.now)

    def __unicode__(self):
        return u'%s. %s' % (self.user.first_name + ' ' + self.user.last_name, self.text)

    class Meta:
        verbose_name = u'Message'
        verbose_name_plural = u'Messages'













