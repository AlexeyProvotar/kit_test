from django import forms
from django.contrib.auth import authenticate

from chat.models import UserModel, Message


class UserCreateForm(forms.ModelForm):
    error_css_class = 'error'
    error_messages = {
        'password_mismatch': 'The two password fields didn`t match.'
        }
    username = forms.CharField(error_messages={'unique': ''})
    email = forms.EmailField(error_messages={'unique': ''})

    password = forms.CharField(label='Password', widget=forms.PasswordInput)
    confirmation = forms.CharField(label='Confirmation', widget=forms.PasswordInput)

    class Meta:
        model = UserModel
        fields = ('username', 'email', 'first_name', 'last_name')

    def clean_confirmation(self):
        password = self.cleaned_data.get('password')
        confirmation = self.cleaned_data.get('confirmation')

        if password and confirmation and password != confirmation:
            raise forms.ValidationError(self.error_messages['password_mismatch'], code='password_mismatch')

        return confirmation

    def save(self, commit=False):
        user = UserModel.objects.create_user(
            username=self.cleaned_data['username'],
            email=self.cleaned_data['email'],
            first_name=self.cleaned_data['first_name'],
            last_name=self.cleaned_data['last_name'],
            password=self.cleaned_data['password']
        )

        user.save()

        return user


class UserLoginForm(forms.Form):
    email = forms.EmailField(label='E-mail', widget=forms.EmailInput(), max_length=255, required=True)
    password = forms.CharField(label='Password', widget=forms.PasswordInput(), required=True)

    def clean(self):
        user = self.login()

        if not user:
            raise forms.ValidationError('That login was invalid. Please try again.')
        else:
            if not user.is_active:
                raise forms.ValidationError('Please verify your email.')

        return self.cleaned_data

    def login(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        user = authenticate(email=email, password=password)

        if not user:
            raise forms.ValidationError('Login or password incorrect. Please try again.')

        return user

    def as_div(self):
        return self._html_output(normal_row='<div class="form_fields" %(html_class_attr)s> %(label)s %(field)s '
                                            '%(help_text)s</div>', error_row='%s', row_ender='</div>',
                                 help_text_html='<span class="help_text">%s</span>', errors_on_separate_row=False)


class MessageForm(forms.ModelForm):
    text = forms.CharField(widget=forms.Textarea(attrs={'class': 'text-input-field'}))

    class Meta:
        model = Message
        exclude = ('user', 'created_at', )




























