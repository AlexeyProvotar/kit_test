from django.contrib import admin

from chat.models import UserModel, Message


@admin.register(UserModel)
class UserAdmin(admin.ModelAdmin):
    fields = ('username', 'email', 'first_name', 'last_name', 'date_joined', 'is_active', 'is_staff', 'registered_at',
              'is_superuser')
    list_display = ('username', 'email', 'first_name', 'last_name', 'date_joined', 'registered_at')
    list_display_links = ('username',)
    search_fields = ('username', 'first_name', 'last_name')


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    fields = ('user', 'text', 'created_at')
    search_fields = ('user', )
