import os

from braces.views import AnonymousRequiredMixin, LoginRequiredMixin, JSONResponseMixin

from django.views.generic import TemplateView, View
from django.views.generic.edit import FormView, CreateView
from django.contrib.auth import logout, login
from django.shortcuts import redirect
from django.http.response import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy, reverse

from chat.forms import UserLoginForm, UserCreateForm, MessageForm
from chat.models import Message

from kit_test import settings


class AnonymousRequiredMixin(AnonymousRequiredMixin):
    def get_authenticated_redirect_url(self):
        return u'/user/'


class ApprovedProfileRequiredMixin(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return self.handle_no_permission(request)

        return super(ApprovedProfileRequiredMixin, self).dispatch(request, *args, **kwargs)


class PrivatePageMixin(ApprovedProfileRequiredMixin):
    pass


class ProtectedView(ApprovedProfileRequiredMixin, FormView):
    form_class = MessageForm

    def get_context_data(self, **kwargs):
        context = super(ProtectedView, self).get_context_data(**kwargs)

        user = self.request.user

        context['user'] = user

        context['messages'] = Message.objects.all().order_by('created_at')

        context['form'] = self.form_class

        return context

    def form_valid(self, form):

        message = form.save(commit=False)
        message.user = self.request.user

        print '[ USER: ', message.user, ']'
        print '[ MESSAGE: ', message.text, ']'
        print '[ DATE CREATED: ', message.created_at, ']'

        if not os.path.exists(settings.LOG_FILE_PATH + '/console.log'):
            file(settings.LOG_FILE_PATH + '/console.log', 'w').close()

        log_file = open(os.path.join(settings.LOG_FILE_PATH, 'console.log'), 'a+')

        log_file.write('\n')
        log_file.write('USER: ' + str(message.user) + '\n')
        log_file.write('MESSAGE: ' + str(message.text) + '\n')
        log_file.write('DATE CREATED: ' + str(message.created_at) + '\n')
        log_file.write('-' * 50)

        log_file.close()

        message.save()

        return HttpResponseRedirect(reverse_lazy('private_homepage'))

    def form_invalid(self, form):

        return HttpResponseRedirect(reverse_lazy('private_homepage'))


class PublicPageView(AnonymousRequiredMixin, TemplateView):
    pass


class IndexPageView(AnonymousRequiredMixin, TemplateView):
    def get_context_data(self, **kwargs):
        context = super(IndexPageView, self).get_context_data(**kwargs)

        context['messages'] = Message.objects.all().order_by('created_at')

        return context


class UserRegisterView(AnonymousRequiredMixin, CreateView):
    form_class = UserCreateForm
    template_name = 'public/signin.html'
    success_url = '/user/register/complete/'

    def form_valid(self, form):
        self.object = form.save()

        return HttpResponseRedirect(self.get_success_url())


class UserLoginView(IndexPageView, FormView):
    form_class = UserLoginForm
    template_name = 'public/login.html'
    success_url = '/user/'

    def get_context_data(self, **kwargs):
        context = super(UserLoginView, self).get_context_data(**kwargs)

        context['form'] = self.form_class

        return context

    def form_valid(self, form):
        user = form.login()

        if user and user.is_active:
            login(self.request, user)

            return HttpResponseRedirect(reverse_lazy('private_homepage'))
        else:

            return HttpResponseRedirect(reverse_lazy('public_homepage'))

    def form_invalid(self, form):
        response = super(UserLoginView, self).form_invalid(form)

        if self.request.is_ajax():
            return JSONResponseMixin(form.errors, status=400)
        else:
            return response


class UserLogoutView(LoginRequiredMixin, View):

    def get(self, request):
        logout(request)

        return redirect(reverse('public_homepage'))